package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    int tempreture;
    String lighting;
    int watering;

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                '}';
    }
}
