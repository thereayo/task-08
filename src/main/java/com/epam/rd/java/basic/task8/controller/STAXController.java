package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowersList;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private FlowersList flowers;

	public FlowersList getFlowers() {
		return flowers;
	}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws XMLStreamException {

		Flower flower = null;
		VisualParameters visualParameters = null;
		GrowingTips growingTips = null;

		// current element name holder
		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);


		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			// handler for start tags
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if ("flowers".equals(currentElement)) {
					flowers = new FlowersList();
					return;
				}

				if ("flower".equals(currentElement)) {
					flower = new Flower();
					return;
				}

				if ("visualParameters".equals(currentElement)) {
					visualParameters = new VisualParameters();
					return;
				}

				if ("growingTips".equals(currentElement)) {
					growingTips = new GrowingTips();
				}
			}

			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				if ("name".equals(currentElement) && flower != null) {
					flower.setName(characters.getData());
					return;
				}
				if ("soil".equals(currentElement) && flower != null) {
					flower.setSoil(characters.getData());
					return;
				}
				if ("origin".equals(currentElement) && flower != null) {
					flower.setOrigin(characters.getData());
					return;
				}
				if ("stemColour".equals(currentElement) && flower != null) {
					visualParameters.setStemColour(characters.getData());
					return;
				}
				if ("leafColour".equals(currentElement) && flower != null) {
					visualParameters.setLeafColour(characters.getData());
					return;
				}
				if ("aveLenFlower".equals(currentElement) && flower != null) {
					visualParameters.setAveLenFlower(Integer.parseInt(characters.getData()));
					return;
				}
				if ("tempreture".equals(currentElement) && flower != null) {
					growingTips.setTempreture(Integer.parseInt(characters.getData()));
					return;
				}
				if ("lighting".equals(currentElement) && flower != null) {
					growingTips.setLighting("yes");
					return;
				}
				if ("watering".equals(currentElement) && flower != null) {
					growingTips.setWatering(Integer.parseInt(characters.getData()));
					return;
				}
				if ("multiplying".equals(currentElement) && flower != null) {
					flower.setMultiplying(characters.getData());
					return;
				}
			}
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if ("flower".equals(localName) && flower != null) {
					flowers.add(flower);
					return;
				}

				// Chars
				if ("visualParameters".equals(localName) && flower != null) {
					flower.setVisualParameters(visualParameters);
					return;
				}

				// Ingredients
				if ("growingTips".equals(localName) && flower != null) {
					flower.setGrowingTips(growingTips);
				}
			}

		}
		// PLACE YOUR CODE HERE
		reader.close();
	}
	public static void main(String[] args) throws Exception {

		STAXController stax = new STAXController(Constants.VALID_XML_FILE);
		stax.parse();

		FlowersList flowers = stax.getFlowers();

		System.out.println("====================================");
		System.out.print("Here is the beers: \n" + flowers);
		System.out.println("====================================");

		DOMController.saveToXML(flowers, "output.stax.xml");
	}
}