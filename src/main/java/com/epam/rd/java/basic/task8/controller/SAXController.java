package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowersList;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
    public static final String TABS = "====================================";
    private String xmlFileName;
    private String currentElement;
    private FlowersList flowers;
    private Flower flower;
    private GrowingTips growingTips;
    private VisualParameters visualParameters;

    public FlowersList getFlowers() {
        return flowers;
    }

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public void parse(boolean validate)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        factory.setNamespaceAware(true);

        if (validate) {
            factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void error(org.xml.sax.SAXParseException e) throws SAXException {
        // if XML document not valid just throw exception
        throw e;
    }
    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        currentElement = localName;

        if ("flowers".equals(currentElement)) {
            flowers = new FlowersList();
            return;
        }

        if ("flower".equals(currentElement)) {
           flower =new Flower();
            return;
        }

        if ("visualParameters".equals(currentElement)) {
            visualParameters = new VisualParameters();
            return;
        }

        if ("growingTips".equals(currentElement)) {
            growingTips = new GrowingTips();
        }
    }
    @Override
    public void characters(char[] ch, int start, int length) {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) {
            return;
        }
        if ("name".equals(currentElement)) {
             flower.setName(elementText);
            return;
        }
        if ("soil".equals(currentElement)) {
            flower.setSoil(elementText);
            return;
        }
        if ("origin".equals(currentElement)) {
            flower.setOrigin(elementText);
            return;
        }
        if ("stemColour".equals(currentElement)) {
            visualParameters.setStemColour(elementText);
            return;
        }
        if ("leafColour".equals(currentElement)) {
            visualParameters.setLeafColour(elementText);
            return;
        }
        if ("aveLenFlower".equals(currentElement)) {
            visualParameters.setAveLenFlower(Integer.parseInt(elementText));
            return;
        }
        if ("tempreture".equals(currentElement)) {
            growingTips.setTempreture(Integer.parseInt(elementText));
            return;
        }
        if ("lighting".equals(currentElement)) {
            growingTips.setLighting("yes");
            return;
        }
        if ("watering".equals(currentElement)) {
            growingTips.setWatering(Integer.parseInt(elementText));
            return;
        }
        if ("multiplying".equals(currentElement)) {
            flower.setMultiplying(elementText);
            return;
        }
    }
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        // Beer
        if ("flower".equals(localName)) {
            flowers.add(flower);
            return;
        }

        // Chars
        if ("visualParameters".equals(localName)) {
            flower.setVisualParameters(visualParameters);
            return;
        }

        // Ingredients
        if ("growingTips".equals(localName)) {
            flower.setGrowingTips(growingTips);
        }
    }

        public static void main(String[] args) throws Exception {

            SAXController sax = new SAXController(Constants.VALID_XML_FILE);

            sax.parse(true);

            FlowersList flowers = sax.getFlowers();

            System.out.println(TABS);
            System.out.print("Here is the test: \n" + flowers);
            System.out.println(TABS);

            sax = new SAXController(Constants.VALID_XML_FILE);
            try {
                sax.parse(true);
            } catch (Exception ex) {
                System.err.println(TABS);
                System.err.println("Validation is failed:\n" + ex.getMessage());
                System.err
                        .println("Try to print test object:" + sax.getFlowers());
                System.err.println(TABS);
            }

            sax.parse(false);

            System.out.println(TABS);
            System.out.print("Here is the test: \n" + sax.getFlowers());
            System.out.println(TABS);

            FlowersList flowers1 = sax.getFlowers();
            DOMController.saveToXML(flowers1, "output.sax.xml");
        }

}