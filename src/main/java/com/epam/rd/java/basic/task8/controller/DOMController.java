package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.comparators.Comparators;
import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowersList;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private FlowersList flowersList;
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
    public FlowersList getFlowersList(){return  flowersList;}
	// PLACE YOUR CODE HERE
	public void parse(boolean validate)
			throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		// make parser validating
		if (validate) {
			// turn validation on
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);

			// turn on xsd validation
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		// set error handler
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				// throw exception if XML document is NOT valid
				throw e;
			}
		});

		// parse XML document
		Document document = db.parse(xmlFileName);

		// get root element
		Element root = document.getDocumentElement();

		// create container
		flowersList = new FlowersList();

		// obtain questions nodes
		NodeList questionNodes = root
				.getElementsByTagName("flower");

		// process questions nodes
		for (int j = 0; j < questionNodes.getLength(); j++) {
			Flower flower = getFlower(questionNodes.item(j));
			// add question to container
			flowersList.add(flower);
		}
	}
	private static Flower getFlower(Node qNode){
		Flower flower=new Flower();
		Element qElement=(Element) qNode;
		Node qtNode = qElement.getElementsByTagName("name")
				.item(0);
		flower.setName(qtNode.getTextContent());
		qtNode = qElement.getElementsByTagName("soil")
				.item(0);
		flower.setSoil(qtNode.getTextContent());
		qtNode = qElement.getElementsByTagName("origin")
				.item(0);
		flower.setOrigin(qtNode.getTextContent());
		flower.setVisualParameters(getVisualParameters(qElement.getElementsByTagName("visualParameters")
				.item(0)));
		flower.setGrowingTips(getGrowingTips(qElement.getElementsByTagName("growingTips")
				.item(0)));
		qtNode = qElement.getElementsByTagName("multiplying")
				.item(0);
		flower.setMultiplying(qtNode.getTextContent());
		return flower;
	}
	private static  VisualParameters getVisualParameters(Node aNode){
    VisualParameters visualParameters = new VisualParameters();
		Element aElement = (Element) aNode;
		Node atNode=aElement.getElementsByTagName("stemColour").item(0);
		visualParameters.setStemColour(atNode.getTextContent());
		atNode=aElement.getElementsByTagName("leafColour").item(0);
		visualParameters.setLeafColour(atNode.getTextContent());
		atNode=aElement.getElementsByTagName("aveLenFlower").item(0);
		visualParameters.setAveLenFlower(Integer.parseInt(atNode.getTextContent()));
		return visualParameters;

	}
   private static  GrowingTips getGrowingTips(Node bNode){
    GrowingTips growingTips=new GrowingTips();
    Element bElement =(Element) bNode;
    Node btNode = bElement.getElementsByTagName("tempreture").item(0);
    growingTips.setTempreture(Integer.parseInt(btNode.getTextContent()));
	   btNode = bElement.getElementsByTagName("lighting").item(0);
	   growingTips.setLighting(String.valueOf(btNode.getAttributes().item(0)));
	   btNode = bElement.getElementsByTagName("watering").item(0);
	   growingTips.setWatering(Integer.parseInt(btNode.getTextContent()));
    return growingTips;
   }
	public static Document getDocument(FlowersList flowers) throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// create root element
		Element tElement = document.createElement("flowers");
        tElement.setAttribute("xmlns","http://www.nure.ua");
		tElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		tElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		// add root element
		document.appendChild(tElement);

        for(Flower flower:flowers){
			Element qElement = document.createElement("flower");
			tElement.appendChild(qElement);


			Element qtElement =
					document.createElement("name");
			qtElement.setTextContent(flower.getName());
			qElement.appendChild(qtElement);
			qtElement =
					document.createElement("soil");
			qtElement.setTextContent(flower.getSoil());
			qElement.appendChild(qtElement);
			qtElement =
					document.createElement("origin");
			qtElement.setTextContent(flower.getOrigin());
			qElement.appendChild(qtElement);

//VP
			qtElement=document.createElement("visualParameters");
			VisualParameters visualParameters=flower.getVisualParameters();

			Element vElement=document.createElement("stemColour");
			vElement.setTextContent(visualParameters.getStemColour());
			qtElement.appendChild(vElement);

			vElement=document.createElement("leafColour");
			vElement.setTextContent(visualParameters.getLeafColour());
			qtElement.appendChild(vElement);
			//ATTRIBUTES
			vElement=document.createElement("aveLenFlower");
			vElement.setAttribute("measure","cm");
			vElement.setTextContent(String.valueOf(visualParameters.getAveLenFlower()));
			qtElement.appendChild(vElement);
			qElement.appendChild(qtElement);

			//GT
			qtElement=document.createElement("growingTips");
			//ATTRIBUTES
			GrowingTips growingTips=flower.getGrowingTips();
			Element gElement=document.createElement("tempreture");
			gElement.setAttribute("measure","celcius");
			gElement.setTextContent(String.valueOf(growingTips.getTempreture()));
			qtElement.appendChild(gElement);
			//watering
			gElement=document.createElement("lighting");
			gElement.setAttribute("lightRequiring","yes");
			qtElement.appendChild(gElement);

			gElement=document.createElement("watering");
			gElement.setAttribute("measure","mlPerWeek");
			gElement.setTextContent(String.valueOf(growingTips.getWatering()));
			qtElement.appendChild(gElement);

			qElement.appendChild(qtElement);
			qtElement =
					document.createElement("multiplying");
			qtElement.setTextContent(flower.getMultiplying());
			qElement.appendChild(qtElement);
			qElement.appendChild(qtElement);
		}
		return document;
	}

	public static void saveToXML(FlowersList flowers, String xmlFileName)
			throws ParserConfigurationException, TransformerException {

		try {
			saveToXML(getDocument(flowers), xmlFileName);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	public static void saveToXML(Document document, String xmlFileName)
			throws TransformerException, FileNotFoundException {

		StreamResult result = new StreamResult(new FileOutputStream(xmlFileName));

		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();

		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		// run transformation
		t.transform(new DOMSource(document), result);
	}
	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
		String xmlFileName="input.xml";
		DOMController dom = new DOMController(Constants.VALID_XML_FILE);
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		FlowersList flowers= domController.getFlowersList();

		Comparators.SortFlowersByName(flowers);

		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
	}

}
